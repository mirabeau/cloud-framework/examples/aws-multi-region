AWS Multi Account
=================

This repository demonstrates the usage of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/) in a single AWS account, multi VPC setup.
Using Ansibe Galaxy, the MCF roles will be installed. Custom roles have been added to provide specific ACL and SG rules. In addition, a custom role has been created to roll out an ECS cluster running AWX services.
The setup includes the following:
* DTAP Account
  - Region eu-west-1
    - Tools VPC
      - VPC - using [aws-vpc](https://gitlab.com/mirabeau/cloud-framework/aws-vpc.git)
      - Route53 Internal Hostedzone - using [aws-hostedzone](https://gitlab.com/mirabeau/cloud-framework/aws-hostedzone.git)
      - Route53 External Hostedzone - using [aws-hostedzone](https://gitlab.com/mirabeau/cloud-framework/aws-hostedzone.git)
      - ECS Cluster - using [aws-ecs-cluster](https://gitlab.com/mirabeau/cloud-framework/aws-ecs-cluster.git)
      - ALB - using [aws-ecs-alb](https://gitlab.com/mirabeau/cloud-framework/aws-ecs-alb.git)
      - AWX ECS resources - using [aws-ecs-awx](https://gitlab.com/mirabeau/cloud-framework/aws-ecs-awx.git)
      - Bastion AutoscalingGroup resources - using [aws-bastion](https://gitlab.com/mirabeau/cloud-framework/aws-bastion.git)
    - Tst VPC
      - VPC - using [aws-vpc](https://gitlab.com/mirabeau/cloud-framework/aws-vpc.git)
      - Route53 Internal Hostedzone - using [aws-hostedzone](https://gitlab.com/mirabeau/cloud-framework/aws-hostedzone.git)
      - Route53 External Hostedzone - using [aws-hostedzone](https://gitlab.com/mirabeau/cloud-framework/aws-hostedzone.git)
  - Region us-east-1
    - Tst VPC
      - VPC - using [aws-vpc](https://gitlab.com/mirabeau/cloud-framework/aws-vpc.git)
      - Route53 Internal Hostedzone - using [aws-hostedzone](https://gitlab.com/mirabeau/cloud-framework/aws-hostedzone.git)
      - Route53 External Hostedzone - using [aws-hostedzone](https://gitlab.com/mirabeau/cloud-framework/aws-hostedzone.git)

Please note that this repository comes with the following ansible.cfg file:
```
[defaults]
hash_behaviour = merge
ansible_connection = local
roles_path = ./roles/galaxy
retry_files_enabled = False
inventory = group_vars/hosts.yml
```

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  
Docker installed on your system and working for your current user.  

Required python modules:
* boto
* boto3
* awscli
* netaddr
* docker

Dependencies
------------
### [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)
* [aws-utils](https://gitlab.com/mirabeau/cloud-framework/aws-utils.git)
* [aws-setup](https://gitlab.com/mirabeau/cloud-framework/aws-setup.git)
* [aws-iam](https://gitlab.com/mirabeau/cloud-framework/aws-iam.git)
* [aws-vpc](https://gitlab.com/mirabeau/cloud-framework/aws-vpc.git)
* [aws-vpc-lambda-cfn-dbprovider](https://gitlab.com/mirabeau/cloud-framework/aws-vpc-lambda-cfn-dbprovider.git)
* [aws-hostedzone](https://gitlab.com/mirabeau/cloud-framework/aws-hostedzone.git)
* [aws-securitygroups](https://gitlab.com/mirabeau/cloud-framework/aws-securitygroups.git)
* [aws-bastion](https://gitlab.com/mirabeau/cloud-framework/aws-bastion.git)
* [aws-ecs-cluster](https://gitlab.com/mirabeau/cloud-framework/aws-ecs-cluster.git)
* [aws-ecs-alb](https://gitlab.com/mirabeau/cloud-framework/aws-ecs-alb.git)
* [aws-ecs-awx](https://gitlab.com/mirabeau/cloud-framework/aws-ecs-awx.git)
* [aws-lambda](https://gitlab.com/mirabeau/cloud-framework/aws-lambda.git)
* [aws-lambda-cfn-secretsprovider](https://gitlab.com/mirabeau/cloud-framework/aws-lambda-cfn-secretsprovider.git)
* [aws-lambda-ecs-instancedrainer](https://gitlab.com/mirabeau/cloud-framework/aws-lambda-ecs-instancedrainer.git)
* [aws-lambda-ecs-monitor](https://gitlab.com/mirabeau/cloud-framework/aws-lambda-ecs-monitor.git)
* [aws-lambda-getondemandprice](https://gitlab.com/mirabeau/cloud-framework/aws-lambda-getondemandprice.git)
* [aws-lambda-route53manager](https://gitlab.com/mirabeau/cloud-framework/aws-lambda-route53manager.git)
* [aws-lambda-setuniquehostname](https://gitlab.com/mirabeau/cloud-framework/aws-lambda-setuniquehostname.git)

Getting started
---------------
### Configuration

#### AWS CLI
Make sure you have your aws profile set with the right IAM access secret and key.
Please refer to [this guide](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html#cli-quick-configuration) if you don't know yet how to set up your credentials.

#### Ansible group_vars
Most of the configuration is already done for you using the yml files in the group_vars directory.
Some variables are really specific to the AWS account though, so please edit the following files and set the correct values for every variable having a value shown between < >.
* group_vars/account/dta.yml
* group_vars/all.yml

### Deployment
Once you've made sure al variables are set correctly, you can start rolling out the environment.
A Makefile has been added so you don't need to type as much, but both the make and the ansible commands are given, so you can choose your own preferred method.

#### Pull MCF roles with Galaxy
To be able to set up this example we need to pull the required roles from with Ansible Galaxy first.
These required roles are already listed in roles/requirements.yml

```bash
make galaxy
# OR
ansible-galaxy install -r roles/requirements.yml -p roles/galaxy/ --force
```

#### Rollout the dtap account
We start with rolling out the dta account
`Make sure your AWS_PROFILE is set to the right profile (export AWS_PROFILE=< your profile name >).`

```bash
make account
# OR
ansible-playbook setup-account.yml -e account=dtap
```
The following CloudFormation Stacks will be deployed:

* iam
* iam-dtap
* lambda-s3
* lambda-cfnsecretsprovider
* lambda-ecsinstancedrainer
* lambda-lambda-ecsmonitor
* lambda-route53manager
* lambda-setuniquehostname

#### Rollout the dtap region eu-west-1
We will then rollout the region in which we want to create our services
```bash
make region=eu-west-1 region
# OR
ansible-playbook setup-region.yml -e account=dta -e region=eu-west-1
```
The following CloudFormation Stacks will be deployed if they don't exist yet:
* setup-s3
* setup
* lambda-s3
* lambda-cfnsecretsprovider
* lambda-ecsinstancedrainer
* lambda-lambda-ecsmonitor
* lambda-route53manager
* lambda-setuniquehostname

#### Rollout test environment in eu-west-1
Next, we can roll out the tst VPC.
```bash
make region=eu-west-1 env=tst environment
# OR
ansible-playbook setup-environment.yml -e account=dta -e region=eu-west-1 -e env=tst
```
The following CloudFormation Stacks will be deployed if they don't exist yet:
* tst-vpc
* tst-acl
* tst-securitygroups
* tst-internal-route53
* tst-external-route53
* tst-securitygroups-ingress

#### Build Docker image for AWX
As we're going to run AWX in our dta tools vpc, we need to build the docker image first, and push it to ECS.

```bash
make region=eu-west-1 env=tool app=awx image=2.1.1 docker-build
# OR
ansible-playbook docker-build.yml -e account=dta -e region=eu-west-1 -e env=tool -e image_tag=2.1.1 --tags awx
```

#### Rollout tools environment in eu-west-1
Now, we can roll out the tool VPC.
```bash
make region=eu-west-1 env=tool environment
# OR
ansible-playbook setup-environment.yml -e account=dta -e region=eu-west-1 -e env=tool
```
The following CloudFormation Stacks will be deployed if they don't exist yet:
* tool-vpc
* tool-acl
* lambda-tool-cfndbprovider
* tool-securitygroups
* tool-internal-route53
* tool-external-route53
* tool-bastion
* tool-securitygroups-ingress

#### Rollout the dtap region us-east-1
We will then rollout the region in which we want to create our services
```bash
make region=us-east-1 region
# OR
ansible-playbook setup-region.yml -e account=dta -e region=us-east-1
```
The following CloudFormation Stacks will be deployed if they don't exist yet:
* setup-s3
* setup
* lambda-s3
* lambda-cfnsecretsprovider
* lambda-route53manager
* lambda-setuniquehostname

#### Rollout test environment in us-east-1
Next, we can roll out the tst VPC.
```bash
make region=us-east-1 env=tst environment
# OR
ansible-playbook setup-environment.yml -e account=dta -e region=us-east-1 -e env=tst
```
The following CloudFormation Stacks will be deployed if they don't exist yet:
* tst-vpc
* tst-acl
* tst-securitygroups
* tst-internal-route53
* tst-external-route53
* tst-securitygroups-ingress


License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>
