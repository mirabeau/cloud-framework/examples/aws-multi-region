AWS_REGION=eu-west-1
NO_COLOR=\033[0m
OK_COLOR=\033[32;01m
ERROR_COLOR=\033[31;01m
WARN_COLOR=\033[33;01m

# Default verbose/readable errors
V=1
AP_0 := @ansible-playbook
AP_1 := @ANSIBLE_STDOUT_CALLBACK=debug ansible-playbook
AP_2 := @ANSIBLE_STDOUT_CALLBACK=debug ansible-playbook -vvv
AP = $(AP_$(V))

galaxy:
	@echo Refreshing galaxy requirements....
	@ansible-galaxy install -r roles/requirements.yml -p roles/galaxy/ --force

.PHONY: account
account:
	@$(AP) setup-account.yml -e account=dtap

.PHONY: region
region:
	@$(AP) setup-region.yml -e account=dtap -e region=${region}

.PHONY: environment
environment:
	@$(AP) setup-environment.yml -e account=dtap -e region=${region} -e env=$(env)

.PHONY: docker-build
docker-build:
	@$(AP) docker-build.yml -e account=dtap -e region=${region} -e env=$(env) image_tag=$(image) --tags $(app)
